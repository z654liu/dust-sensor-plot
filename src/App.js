import React, { useEffect, useState } from 'react';
import DateTimeRangePicker from '@wojtekmaj/react-datetimerange-picker';
import { Menu, Dropdown, Button } from 'antd';
import './App.css';
import 'react-calendar/dist/Calendar.css';
import "antd/dist/antd.css";
import LineChart from './components/line-chart/line-chart.component';
import { TIME_ZONE, TIME_ZONES_MAP } from './utils/timezones';


const App = () => {
  const [values, setValues] = useState([]);
  const [displayDateRange, setDisplayDateRange] = useState([new Date(new Date().toLocaleString('en-US', { timeZone:  'America/Mexico_City' })),
                                                                      new Date(new Date().toLocaleString('en-US', { timeZone:  'America/Mexico_City' }))]);
  const [realDateRange, setRealDateRange] = useState([new Date(new Date().toLocaleString('en-US', { timeZone:  'America/Mexico_City' })),
                                                                new Date(new Date().toLocaleString('en-US', { timeZone:  'America/Mexico_City' }))]);
  const [lastUpdatedTime, setLastUpdatedTime] = useState(new Date());
  const [sheetNames, setSheetNames] = useState([]);
  const [sheetName, setSheetName] = useState('e00fce689c24ee215bea2757');
  const [timeZonesMap, setTimeZonesMap] = useState(TIME_ZONES_MAP);
  const [timeZone, setTimeZone] = useState(TIME_ZONE);


  useEffect(() => {
    fetch('https://sheets.googleapis.com/v4/spreadsheets/1QbbeWXxKc1vQP-2UPwsouafyRDZ70Lu1A7QdXk5YbBc/?key=AIzaSyAQuSY5SSe5I3nAGwPIYDH8UI8t5VMdoW0')
        .then(response => response.json())
        .then(result => {
          const tempSheetNames = result.sheets.map(sheet => sheet.properties.title);
          setSheetNames(tempSheetNames);
          setSheetName(tempSheetNames[0]);
        });
  }, []);

  useEffect(() => {
    fetch('https://sheets.googleapis.com/v4/spreadsheets/1QbbeWXxKc1vQP-2UPwsouafyRDZ70Lu1A7QdXk5YbBc/values:batchGet?key=AIzaSyAQuSY5SSe5I3nAGwPIYDH8UI8t5VMdoW0&ranges=' + sheetName)
        .then(response => response.json())
        .then(result => {
          const tempValues = result.valueRanges[0].values;
          setValues(tempValues);
          setLastUpdatedTime(new Date(tempValues[tempValues.length - 1][0]));
        });
  }, [sheetName]);

  useEffect(() => {
    const startDate = new Date(realDateRange[0]);
    const endDate = new Date(realDateRange[1]);
    setDisplayDateRange([new Date(startDate.setHours(startDate.getHours() + timeZonesMap[timeZone])),
                               new Date(endDate.setHours(endDate.getHours() + timeZonesMap[timeZone]))]);
  }, [timeZone]);


  const onClickSheetName = ({ key }) => {
    setSheetName(key);
  };

  const onClickTimeZone = ({ key }) => {
    setTimeZone(key);
  };

  const setDisplayAndRealDateRange = (value) => {
    setDisplayDateRange(value);

    const startDate = new Date(value[0]);
    const endDate = new Date(value[1]);
    setRealDateRange([new Date(startDate.setHours(startDate.getHours() + timeZonesMap[timeZone])),
                            new Date(endDate.setHours(endDate.getHours() + timeZonesMap[timeZone]))]);
  };


  const dropDownMenuSheetNames = (
    <Menu onClick={onClickSheetName}>
      {sheetNames.map(sheetName =>
        <Menu.Item key={sheetName}>
          {sheetName}
        </Menu.Item>
      )}
    </Menu>
  );

  const dropDownMenuTimeZones = (
      <Menu onClick={onClickTimeZone}>
        {Object.keys(timeZonesMap).map(timeZone =>
            <Menu.Item key={timeZone}>
              {timeZone}
            </Menu.Item>
        )}
      </Menu>
  );


  return (
      <div>
        <div>
          <LineChart
              values={values}
              dateRange={realDateRange}
              timeZonesMap={timeZonesMap}
              timeZone={timeZone}
          />
        </div>

        <div className='container'>
          <DateTimeRangePicker
              onChange={setDisplayAndRealDateRange}
              value={displayDateRange}
              maxDate={new Date()}
              required={true}
          />
        </div>

        <div className='container'>
          <Dropdown
              overlay={dropDownMenuSheetNames}
              placement="bottom"
              arrow>
            <Button onClick={e => e.preventDefault()}>
              {sheetName}
            </Button>
          </Dropdown>
        </div>

        <div className='container'>
          <Dropdown
              overlay={dropDownMenuTimeZones}
              placement="bottom"
              arrow>
            <Button onClick={e => e.preventDefault()}>
              {timeZone}
            </Button>
          </Dropdown>
        </div>

        <p className='right'>
          {"Last Updated: " + new Date(new Date(lastUpdatedTime).setHours(new Date(lastUpdatedTime).getHours() + timeZonesMap[timeZone])).toLocaleString()}
        </p>
      </div>
  );
}

export default App;
