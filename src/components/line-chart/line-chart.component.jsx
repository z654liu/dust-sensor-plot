import React  from 'react';
import ReactEcharts from 'echarts-for-react'


const LineChart = ({ values, dateRange, timeZone, timeZonesMap }) => {
  const lineOption = getLineOption(values, dateRange, timeZone, timeZonesMap);

  return (
      <ReactEcharts option={lineOption} />
  );
}

const getLineOption = (values, dateRange, timeZone, timeZonesMap) => {
  values = values.slice(1);
  const startDate = dateRange[0];
  const endDate = dateRange[1];
  const dateList = values.map(value => new Date(value[3]));
  const startIndex = findStartIndex(startDate, dateList);
  const endIndex = findEndIndex(endDate, dateList);
  const selectedValues = values.slice(startIndex, endIndex + 1);

  const xAxisData = [];
  const PM1DataList = [];
  const PM2DataList = [];
  const PM10DataList = [];

  for (const value of selectedValues) {
    let date = new Date(value[3]);
    date = new Date(date.setHours(date.getHours() + timeZonesMap[timeZone]));
    const month = date.getMonth() + 1;
    const day = date.getDate();
    const hour = date.getHours();
    let minute = date.getMinutes();

    if (Number(minute) < 10) {
      minute = '0' + minute;
    }
    xAxisData.push(month + '/' + day + ' ' + hour + ':' + minute);

    PM1DataList.push(Number(value[4]));
    PM2DataList.push(Number(value[5]));
    PM10DataList.push(Number(value[6]));
  }

  const xAxis = {
    type: 'category',
    data: xAxisData
  };
  const yAxis = {
    type: 'value'
  };
  const PM1DataObject = {
    name: 'PM1.0',
    type: 'line',
    data: PM1DataList
  };
  const PM2DataObject = {
    name: 'PM2.5',
    type: 'line',
    data: PM2DataList
  };
  const PM10DataObject = {
    name: 'PM10',
    type: 'line',
    data: PM10DataList
  };
  const seriesList = [PM1DataObject, PM2DataObject, PM10DataObject];

  return {
    legend: {
      data: ["PM1.0", "PM2.5", "PM10"]
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        label: {
          show: true
        }
      }
    },
    xAxis: xAxis,
    yAxis: yAxis,
    series: seriesList
  };
}

const findStartIndex = (startDate, dateList) => {
  startDate = startDate.getTime();
  let low = 0;
  let high = dateList.length - 1;

  while (low <= high) {
    const mid = Math.floor(low + (high - low) / 2);
    const currentDate = dateList[mid].getTime();

    if (startDate <= currentDate) {
      if ((mid === 0) || (startDate > currentDate)) {
        return mid;
      }
      else {
        high = mid - 1;
      }
    }
    else {
      low = mid + 1;
    }
  }

  return low;
}

const findEndIndex = (endDate, dateList) => {
  endDate = endDate.getTime();
  let low = 0;
  let high = dateList.length - 1;
  const dateListLength = dateList.length - 1;

  while (low <= high) {
    const mid = Math.floor(low + (high - low) / 2);

    if (endDate < dateList[mid].getTime()) {
      high = mid - 1;
    }
    else {
      if ((mid === dateListLength) || (endDate < dateList[mid + 1].getTime())) {
        return mid;
      }
      else {
        low = mid + 1;
      }
    }
  }

  return low;
}


export default LineChart;